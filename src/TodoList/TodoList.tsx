import React from "react";
import { useState, useEffect } from "react";

type Props = {
  todos: [];
};

type todo = {
  id: number;
  title: string;
};

function TodoList(props: Props) {
  const [todos, setTodos] = useState([]);

  useEffect(() => {
    setTodos(props.todos);
  }, [props.todos]);

  const HandleDelete = (id: number) => {
    const newTodos = todos.filter((item: todo) => (item.id !== id));
    localStorage.setItem("todoList", JSON.stringify(newTodos));
    setTodos(newTodos);
  };

  return (
    <div>
      <ul>
        {todos.map((item: todo) => (
          <li key={item.id} onClick={() => HandleDelete(item.id)}>
             {item.title}
          </li>
        ))}
      </ul>
    </div>
  );
}

export default TodoList;
