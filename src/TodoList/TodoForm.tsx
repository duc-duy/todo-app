import React from "react";
import { useState, useEffect } from "react";
import TodoList from "./TodoList";

const todoList = [
  { id: 1, title: "I love Easy Frontend! 😍 " },
  { id: 2, title: "We love Easy Frontend! 🥰 " },
  { id: 3, title: "They love Easy Frontend! 🚀 " },
];

function TodoForm() {
  const [todos, setTodos] = useState(
    JSON.parse(localStorage.getItem("todoList")!) || todoList
  );
  const [todo, setTodo] = useState("");

  useEffect(() => {
    if (localStorage.getItem("todoList") === null) {
      localStorage.setItem("todoList", JSON.stringify(todoList));
    }
  }, [todos]);

  const HandleSubmit = () => {
    if (todo) {
      const newTodos = JSON.parse(localStorage.getItem("todoList")!) || [];
      newTodos.push({
        id: newTodos.length > 0 ? newTodos[newTodos.length - 1].id + 1 : 1,
        title: todo,
      });
      localStorage.setItem("todoList", JSON.stringify(newTodos));
      setTodos(newTodos);
      setTodo("");
    } else {
      alert("Vui lòng nhập todo...");
    }
  };
  return (
    <form action="#">
      <input
        type="text"
        placeholder="Nhập todo...."
        onChange={(e) => setTodo(e.target.value)}
        value={todo}
      />
      <button title="submit" onClick={HandleSubmit}>
        AddTodo
      </button>
      <TodoList todos={todos} />
    </form>
  );
}

export default TodoForm;
